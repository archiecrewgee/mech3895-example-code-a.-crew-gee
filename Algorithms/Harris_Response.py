import numpy as np

# finds harris response of an array
def find_harris_response(image, k):
    sobelDerivative_x = [0,1,2,1,0,-1,-2,-1,0]  # sobel derivative in x direction | [[1, 2, 1], [0, 0, 0], [-1, -2, -1]]
    sobelDerivative_y = [0,1,0,-1,-2,-1,0,1,2]  # sobel derivative in y direction | [[1, 0, -1], [2, 0, -2], [1, 0, -1]]


    pad = pad_image(image)  # pad image
    intensity_x = sum_of_squared_differences(pad, sobelDerivative_x)
    intensity_y = sum_of_squared_differences(pad, sobelDerivative_y)

    intensity_x_2 = intensity_x * intensity_x   # find intensityX ^ 2
    intensity_y_2 = intensity_y * intensity_y   # find intensityY ^ 2
    #intensity_x_y = intensity_x * intensity_y   # find intensityX * intensityY

    response = np.zeros((np.shape(image)))  # create response array
    # find response value for all values
    response = (intensity_x_2 * intensity_y_2) - (k * ((intensity_x_2 + intensity_y_2)**2))

    return response

# performs the sum of squared differences in a 3 by 3 window around every pixel
def sum_of_squared_differences(image, sobelDerivative):
    # create 'wobbled' image
    image_3D = image[:,:,np.newaxis]
    shiftType = ['up_left','up','up_right','right','down_right','down','down_left','left']
    # create 3 dimensional array of wobbled image in all directions
    for i in range(8):
        image_3D = np.dstack((image_3D, shift_array(image, shiftType[i])))

    # create 3D array of sobel derivative
    dummyArray = np.zeros((1,1,9))
    dummyArray[0][0] = sobelDerivative
    dummyIntensity = np.zeros((np.shape(image_3D)[0],np.shape(image_3D)[1]))
    # multiply each pixel of the image (9 dimensions) by the 9 dimensional sobel derivative
    dummyIntensity = np.sum(image_3D * dummyArray, axis=2)
    # Remove edges of intensity as the padding is not needed any more
    intensity = dummyIntensity[1:-1,1:-1]
    return intensity

# adds rim of zeros around array as padding
def pad_image(image):
    dummyImage = np.zeros((np.shape(image)[0] + 2, np.shape(image)[1] + 2))
    dummyImage[1 : np.shape(image)[0] + 1, 1 : np.shape(image)[1] + 1] = image
    return dummyImage

# shifts array by 1 in any direction
def shift_array(array, type):
    shifted = np.zeros((np.shape(array)))
    if type == 'up_right': shifted[:-1,1:] = array[1:,:-1]    # shift up right
    elif type == 'up': shifted[:-1,:] = array[1:,:]     # shift up
    elif type == 'up_left': shifted[:-1,:-1] = array[1:,1:]       # shift up left
    elif type == 'left': shifted[:,:-1] = array[:,1:]
    elif type == 'down_left': shifted[1:,:-1] = array[:-1,1:]
    elif type == 'down': shifted[1:,:] = array[:-1,:]
    elif type == 'down_right': shifted[1:,1:] = array[:-1,:-1]
    elif type == 'right': shifted[:,1:] = array[:,:-1]

    return shifted
