import numpy as np

class Stamp():
    def __init__(self, pointsArray_):
        self.count = len(pointsArray_[0])

        self.points = np.array(list(zip(pointsArray_[0], pointsArray_[1])))
        self.minRow = np.min(pointsArray_[0])
        self.maxRow = np.max(pointsArray_[0])
        self.rangeRow = self.maxRow - self.minRow
        self.minColumn = np.min(pointsArray_[1])
        self.maxColumn = np.max(pointsArray_[1])
        self.rangeColumn = self.maxColumn - self.minColumn

        self.row = np.sum(pointsArray_[0])/self.count
        self.column = np.sum(pointsArray_[1])/self.count

    def create_image(self):
        self.image = np.zeros((self.rangeRow + 1, self.rangeColumn + 1))
        offsetPoints = self.points[:] - [self.minRow, self.minColumn]
        for i in range(np.shape(self.points)[0]):
            self.image[offsetPoints[i][0],offsetPoints[i][1]] = 1
        return self.image

    def correct_image(self):
        self.dilate_image()
        self.dilate_image()
        self.erode_image()

    def dilate_image(self):
        image_3D = create_3D_image(self.image)
        self.image = np.where(np.bitwise_and(self.image == 0, np.sum(image_3D, axis = 2) > 5), 1, self.image)

    def erode_image(self):
        image_3D = create_3D_image(self.image)
        self.image = np.where(np.bitwise_and(self.image == 1, np.sum(image_3D, axis=2) > 5), 1, 0)

    def validate_stamp(self, rowValues, columnValues):
        if (rowValues[0] < self.rangeRow < rowValues[1]) and (columnValues[0] < self.rangeColumn < columnValues[1]):
            return True
        return False

    def offset_values(self, row , column):
        self.minRow += row
        self.maxRow += row
        self.row    += row
        self.minColumn += column
        self.maxColumn += column
        self.column    += column

def create_3D_image(image):
    image_3D = image[:, :, np.newaxis]
    shiftType = ['up_left', 'up', 'up_right', 'right', 'down_right', 'down', 'down_left', 'left']
    # create 3 dimensional array of wobbled image in all directions
    for i in range(8):
        image_3D = np.dstack((image_3D, shift_array(image, shiftType[i])))
    return image_3D

def shift_array(array, type):
    shifted = np.zeros((np.shape(array)))
    if type == 'up_right': shifted[:-1,1:] = array[1:,:-1]    # shift up right
    elif type == 'up': shifted[:-1,:] = array[1:,:]     # shift up
    elif type == 'up_left': shifted[:-1,:-1] = array[1:,1:]       # shift up left
    elif type == 'left': shifted[:,:-1] = array[:,1:]
    elif type == 'down_left': shifted[1:,:-1] = array[:-1,1:]
    elif type == 'down': shifted[1:,:] = array[:-1,:]
    elif type == 'down_right': shifted[1:,1:] = array[:-1,:-1]
    elif type == 'right': shifted[:,1:] = array[:,:-1]

    return shifted