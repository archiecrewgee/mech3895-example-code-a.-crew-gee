import numpy as np
import math
import Text_Identification as txt

def normalise_dimensions(array, boxLength):
    rowMultiplier = np.shape(array)[0] / boxLength
    columnMultiplier = np.shape(array)[1] / boxLength

    newArray = np.zeros((boxLength, boxLength))
    for i_row in range(boxLength):
        for i_column in range(boxLength):
            newArray[i_row][i_column] = array[int(math.floor(i_row * rowMultiplier))][int(math.floor(i_column * columnMultiplier))]

    return newArray

def first_order_spatial_derivatives(array):
    sobelDerivative_x = [0, 1, 2, 1, 0, -1, -2, -1, 0]  # sobel derivative in x direction | [[1, 2, 1], [0, 0, 0], [-1, -2, -1]]
    sobelDerivative_y = [0, 1, 0, -1, -2, -1, 0, 1, 2]  # sobel derivative in y direction | [[1, 0, -1], [2, 0, -2], [1, 0, -1]]

    pad = txt.pad_image(array)  # pad image
    intensity_x = txt.sum_of_squared_differences(pad, sobelDerivative_x)
    intensity_y = txt.sum_of_squared_differences(pad, sobelDerivative_y)

    columnDerivatives = (intensity_x).flatten()
    rowDerivatives = (intensity_y).flatten()

    return rowDerivatives, columnDerivatives

def distance_map_features(array, shape):
    # create harris response
    harrisResponse = txt.find_harris_response(np.where(array > np.sum(array) / np.shape(array.flatten())[0], 1, 0), 0.05)
    # find edges
    edgePositions = np.where(harrisResponse > -1)
    # find euclidean distance
    distanceArray = []
    for i_row in range(shape):
        for i_column in range(shape):
            minDistance = 100
            for i_edge in range(len(edgePositions[0])):
                euclidianDistance = distance([i_row, i_column], [edgePositions[0][i_edge], edgePositions[1][i_edge]])
                if euclidianDistance < minDistance:
                    minDistance = euclidianDistance
            distanceArray.append(minDistance)

    return distanceArray

def constant_gradient_varience_features(array):
    image_3D = array[:, :, np.newaxis]
    shiftType = ['up_left', 'up', 'up_right', 'right', 'down_right', 'down', 'down_left', 'left']
    # create 3 dimensional array of wobbled image in all directions
    for i in range(8): image_3D = np.dstack((image_3D, txt.shift_array(array, shiftType[i])))
    # find local mean from 3d array
    local_mean = (np.sum(image_3D, axis = 2) / (len(shiftType) + 1)) + 0.1
    # find greyscale value divided by local mean and return them
    results = array / local_mean
    return results.flatten()

def distance(a, b):
    xChange = a[0] - b[0]
    yChange = a[1] - b[1]
    return math.sqrt((xChange**2) + (yChange**2))

def discrete_cosine_transform(array, shape):
    F = np.zeros((shape, shape))
    multiplier = 2 * math.sqrt(2 / shape)
    for u in range(shape):
        for v in range(shape):
            F[u][v] = (multiplier**2) * DCT_sum(u, v, shape, array)

    return F.flatten()

def DCT_sum(u, v, shape, array):
    sum = 0
    precalculated_U = math.pi * u / (2 * shape)
    precalculated_V = math.pi * v / (2 * shape)
    for i_row in range(shape):
        for i_column in range(shape):
            sum += delta_e(i_row) * delta_e(i_column) * math.cos(precalculated_U * (2 * i_row + 1)) * math.cos(precalculated_V * (2 * i_column + 1)) * array[i_row][i_column]
    return sum

def delta_e(e):
    if e == 0: return 1/math.sqrt(0.5)
    else: return 1
