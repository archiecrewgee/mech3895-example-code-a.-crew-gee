import Data_Preprocessing
import numpy as np
from csv import reader

class Multilayer_Perceptron():
    def __init__(self, sizes, beta, momentum):
        self.beta = beta
        self.momentum = momentum

        self.nin = sizes[0]  # number of features in each sample
        self.nhidden1 = sizes[1]  # number of neurons in the first hidden layer
        self.nhidden2 = sizes[2]  # number of neurons in the second hidden layer
        self.nout = sizes[3]  # number of classes / the number of neurons in the output layer

        # Pre output softmax layer
        self.outputs_no_softmax = 0;

        # Initialise the network of two hidden layers
        self.weights1 = (np.random.rand(self.nin + 1, self.nhidden1) - 0.5) * 2 / np.sqrt(self.nin)  # hidden layer 1
        self.weights2 = (np.random.rand(self.nhidden1 + 1, self.nhidden2) - 0.5) * 2 / np.sqrt(self.nhidden1)  # hidden layer 2
        self.weights3 = (np.random.rand(self.nhidden2 + 1, self.nout) - 0.5) * 2 / np.sqrt(self.nhidden2)  # output layer

        # class variables for previous weights to use with momentum factor
        self.prevWeights1 = np.zeros(np.shape(self.weights1))
        self.prevWeights2 = np.zeros(np.shape(self.weights2))
        self.prevWeights3 = np.zeros(np.shape(self.weights3))

    def forwardPass(self, inputs):
        # layer 1
        # compute the forward pass on the first hidden layer with the sigmoid function
        #print(-self.beta)
        #print(np.exp((inputs @ self.weights1)))

        self.hidden1 = 1 / (1 + np.exp(-self.beta * (inputs @ self.weights1)))

        # array of -1 for the offsets
        dummyArray = -np.ones((np.shape(self.hidden1)[0], 1))

        # add offset to back of hidden 1
        self.hidden1 = np.hstack((self.hidden1, dummyArray))

        # layer 2
        # compute the forward pass on the second hidden layer with the sigmoid function
        self.hidden2 = 1 / (1 + np.exp(-self.beta * (self.hidden1 @ self.weights2)))
        self.hidden2 = np.hstack((self.hidden2, dummyArray))

        # output layer
        # compute the forward pass on the output layer with softmax function
        finalLayerDotProduct = self.hidden2 @ self.weights3  # get dot product of hidden layer 2 and weights
        exponentials = np.exp(finalLayerDotProduct - finalLayerDotProduct.max())  # get the offset e^x values
        outputs = np.zeros((np.shape(finalLayerDotProduct)[0], np.shape(self.weights3)[1]))  # define output array
        sums = np.sum(exponentials, axis=1)  # sum each row
        for i in range(0, np.shape(outputs)[0]):
            outputs[i] = exponentials[i] / sums[i]  # add rows to the output table

        return outputs

    def run(self, inputs):
        inputs = np.concatenate((inputs, -np.ones((np.shape(inputs)[0], 1))), axis=1)
        return self.forwardPass(inputs)

    def evaluate(self, X, y):
        inputs = np.concatenate((X, -np.ones((np.shape(X)[0], 1))), axis=1)
        outputs = self.forwardPass(inputs)
        nclasses = np.shape(y)[1]

        # 1-of-N encoding
        outputs = np.argmax(outputs, 1)
        targets = np.argmax(y, 1)

        cm = np.zeros((nclasses, nclasses))
        for i in range(nclasses):
            for j in range(nclasses):
                cm[i, j] = np.sum(np.where(outputs == i, 1, 0) * np.where(targets == j, 1, 0))

        print("The confusion matrix is:")
        print(cm)
        print("The accuracy is ", np.trace(cm) / np.sum(cm) * 100)

    def set_weights(self, weights):
        self.weights1 = weights[0]
        self.weights2 = weights[1]
        self.weights3 = weights[2]


def create_sample_from_array(array):
    normalisedArray = Data_Preprocessing.normalise_dimensions(array, 16) / 256
    rowDerivatives, columnDerivatives = Data_Preprocessing.first_order_spatial_derivatives(normalisedArray)
    distanceArray = Data_Preprocessing.distance_map_features(normalisedArray, 16)
    gradientFeatures = Data_Preprocessing.constant_gradient_varience_features(normalisedArray)
    discreteCosineFeatures = Data_Preprocessing.discrete_cosine_transform(normalisedArray, 16)
    discreteCosineFeatures = discreteCosineFeatures / 100
    combinedSample = np.hstack((rowDerivatives, columnDerivatives, distanceArray, gradientFeatures, discreteCosineFeatures))
    return combinedSample

def convert_file_to_2D_array(name):
    file = open(name, newline = '\n')
    line = reader(file)
    output = []
    for dummyLine in line:
        output.append(dummyLine[:-1])
    file.close()
    return np.array(output).astype(float)