import numpy as np
import Data_Preprocessing
import random

class Multilayer_Perceptron():
    def __init__(self, sizes, beta, momentum):
        self.beta = beta
        self.momentum = momentum

        self.nin = sizes[0]  # number of features in each sample
        self.nhidden1 = sizes[1]  # number of neurons in the first hidden layer
        self.nhidden2 = sizes[2]  # number of neurons in the second hidden layer
        self.nout = sizes[3]  # number of classes / the number of neurons in the output layer

        # Pre output softmax layer
        self.outputs_no_softmax = 0;

        # Initialise the network of two hidden layers
        self.weights1 = (np.random.rand(self.nin + 1, self.nhidden1) - 0.5) * 2 / np.sqrt(self.nin)  # hidden layer 1
        self.weights2 = (np.random.rand(self.nhidden1 + 1, self.nhidden2) - 0.5) * 2 / np.sqrt(self.nhidden1)  # hidden layer 2
        self.weights3 = (np.random.rand(self.nhidden2 + 1, self.nout) - 0.5) * 2 / np.sqrt(self.nhidden2)  # output layer

        # class variables for previous weights to use with momentum factor
        self.prevWeights1 = np.zeros(np.shape(self.weights1))
        self.prevWeights2 = np.zeros(np.shape(self.weights2))
        self.prevWeights3 = np.zeros(np.shape(self.weights3))

    def train(self, inputs, targets, eta, niterations):
        ndata = np.shape(inputs)[0]  # number of data samples
        # adding the bias
        inputs = np.concatenate((inputs, -np.ones((ndata, 1))), axis=1)

        # numpy array to store the update weights
        updatew1 = np.zeros((np.shape(self.weights1)))
        updatew2 = np.zeros((np.shape(self.weights2)))
        updatew3 = np.zeros((np.shape(self.weights3)))

        for n in range(niterations):
            # forward phase
            self.outputs = self.forwardPass(inputs)

            # Error using the sum-of-squares error function
            error = 0.5 * np.sum((self.outputs - targets) ** 2)

            if (np.mod(n, 50) == 0):
                print("Iteration: ", n, " Error: ", error)

            # backward phase
            # Compute the derivative of the output layer. NOTE: you will need to compute the derivative of
            # the softmax function. Hints: equation 4.55 in the book.
            deltao = (self.outputs - targets) / np.shape(self.outputs)[0]
            for image in range(np.shape(self.outputs)[0]):
                diagonal = np.diag(self.outputs[image] * (1 - self.outputs[image]))
                square = np.vstack(self.outputs[image]) * self.outputs[image]
                dummyArray = np.where(diagonal == 0, square, diagonal)
                deltao[image] = deltao[image] * np.sum(dummyArray, axis=0)

            # compute the derivative of the second hidden layer
            deltah2 = (deltao @ np.transpose(self.weights3)) * (self.hidden2 * self.beta * (1 - self.hidden2))

            # compute the derivative of the first hidden layer
            deltah1 = (deltah2[:, :-1] @ np.transpose(self.weights2)) * (self.hidden1 * self.beta * (1 - self.hidden1))

            # update the weights of the three layers: self.weights1, self.weights2 and self.weights3
            # here you can update the weights as we did in the week 4 lab (using gradient descent)
            # but you can also add the momentum
            updatew1 = eta * (np.transpose(inputs) @ deltah1[:, :-1]) + (self.momentum * self.prevWeights1)
            updatew2 = eta * (np.transpose(self.hidden1) @ deltah2[:, :-1]) + (self.momentum * self.prevWeights2)
            updatew3 = eta * (np.transpose(self.hidden2) @ deltao) + (self.momentum * self.prevWeights3)

            self.prevWeights1 = updatew1
            self.prevWeights2 = updatew2
            self.prevWeights3 = updatew3

            self.weights1 -= updatew1
            self.weights2 -= updatew2
            self.weights3 -= updatew3


    def forwardPass(self, inputs):
        # layer 1
        # compute the forward pass on the first hidden layer with the sigmoid function
        #print(-self.beta)
        #print(np.exp((inputs @ self.weights1)))

        self.hidden1 = 1 / (1 + np.exp(-self.beta * (inputs @ self.weights1)))

        # array of -1 for the offsets
        dummyArray = -np.ones((np.shape(self.hidden1)[0], 1))

        # add offset to back of hidden 1
        self.hidden1 = np.hstack((self.hidden1, dummyArray))

        # layer 2
        # compute the forward pass on the second hidden layer with the sigmoid function
        self.hidden2 = 1 / (1 + np.exp(-self.beta * (self.hidden1 @ self.weights2)))
        self.hidden2 = np.hstack((self.hidden2, dummyArray))

        # output layer
        # compute the forward pass on the output layer with softmax function
        finalLayerDotProduct = self.hidden2 @ self.weights3  # get dot product of hidden layer 2 and weights
        exponentials = np.exp(finalLayerDotProduct - finalLayerDotProduct.max())  # get the offset e^x values
        outputs = np.zeros((np.shape(finalLayerDotProduct)[0], np.shape(self.weights3)[1]))  # define output array
        sums = np.sum(exponentials, axis=1)  # sum each row
        for i in range(0, np.shape(outputs)[0]):
            outputs[i] = exponentials[i] / sums[i]  # add rows to the output table

        return outputs


    def evaluate(self, X, y):
        inputs = np.concatenate((X, -np.ones((np.shape(X)[0], 1))), axis=1)
        outputs = self.forwardPass(inputs)
        nclasses = np.shape(y)[1]

        # 1-of-N encoding
        outputs = np.argmax(outputs, 1)
        targets = np.argmax(y, 1)

        cm = np.zeros((nclasses, nclasses))
        for i in range(nclasses):
            for j in range(nclasses):
                cm[i, j] = np.sum(np.where(outputs == i, 1, 0) * np.where(targets == j, 1, 0))

        print("The confusion matrix is:")
        print(cm)
        print("The accuracy is ", np.trace(cm) / np.sum(cm) * 100)

